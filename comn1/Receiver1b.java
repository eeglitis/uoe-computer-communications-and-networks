/* Edgars Eglitis 1353184 */
// ACK implementation consists of simply sending back the sequence number

import java.io.*;
import java.net.*;

public class Receiver1b {
	
	static boolean printer = false; // flag for various printing statements
	
	public static void main(String[] args) throws IOException {
		
		// open a new socket on the provided port
		int port = Integer.parseInt(args[0]);
		DatagramSocket serverSocket = new DatagramSocket(port);
		if (printer) System.out.format("Listening in on port %d:\n", port);
		
		// prepare the input packet buffer
		int packetSize = 1024;
        byte[] data = new byte[packetSize+3];
        DatagramPacket packet = new DatagramPacket(data, data.length);
        
        // set ack size and last packet received
        byte[] ackData = new byte[2];
        int lastSeqNo = -1;
        
        // prepare file to be written to
        File file = new File(args[1]);
		OutputStream os = new FileOutputStream(file);
	    DataOutputStream dos = new DataOutputStream(os);
        
        while(true) {
        	
        	// receive the packet
            serverSocket.receive(packet);
            data = packet.getData();
            
            // only write if the sequence numbers are different
            if ((int) data[1]!=lastSeqNo) {
            	dos.write(data, 3, packet.getLength()-3);
            	if (printer) System.out.format("Received packet %d...\n", (int) data[1]);
            	lastSeqNo = (int) data[1];
            }
            
            // prepare and send the ack
            ackData[0] = data[0];
            ackData[1] = data[1];
            DatagramPacket ack = new DatagramPacket(ackData, ackData.length, packet.getAddress(), packet.getPort());
            serverSocket.send(ack);
            if (printer) System.out.format("Sent ACK for packet %d to port %d of %s...\n", (int) data[1], packet.getPort(), packet.getAddress());
            
            // if last flag is set, close socket
            if (data[2] == 1) {
            	serverSocket.close();
            	if (printer) System.out.println("All packets received.");
            	break;
            }
        }
        dos.close();
	}
}