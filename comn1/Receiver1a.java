/* Edgars Eglitis 1353184 */

import java.io.*;
import java.net.*;

public class Receiver1a {
	
	static boolean printer = false; // flag for various printing statements
	
	public static void main(String[] args) throws IOException {
		
		// open a new socket on the provided port
		int port = Integer.parseInt(args[0]);
		DatagramSocket serverSocket = new DatagramSocket(port);
		if (printer) System.out.format("Listening in on port %d:\n", port);
		
		// prepare the input packet buffer
		int packetSize = 1024;
        byte[] data = new byte[packetSize+3];
        DatagramPacket packet = new DatagramPacket(data, data.length);
        
        // prepare file to be written to
        File file = new File(args[1]);
		OutputStream os = new FileOutputStream(file);
	    DataOutputStream dos = new DataOutputStream(os);
        
        while(true) {
        	
        	// receive packet and write its data to the file
            serverSocket.receive(packet);
            data = packet.getData();
            dos.write(data, 3, packet.getLength()-3); 						// start writing from index 3, as 0-1 are sequence numbers and 2 is last packet flag
            int sequenceNo = (data[0] & 0xFF) | ((data[1] & 0xFF) << 8); 	// reconstruct sequence number from the two bytes
            if (printer) System.out.format("Received packet %d...\n", sequenceNo);
            
            // if the last flag is set, close the socket
            if (data[2] == 1) {
            	serverSocket.close();
            	if (printer) System.out.println("All packets received.");
            	break;
            }
        }
        dos.close();
	}
}