/* Edgars Eglitis 1353184 */

import java.net.*;
import java.io.*;

public class Sender1a {
	
	static boolean printer = false; // flag for various printing statements
	
	public static void main(String[] args) throws Exception {
		
		// read the entire file as a byte array
		File file = new File(args[2]);
		byte[] source = new byte[(int) file.length()];
		InputStream is = new FileInputStream(file);
	    DataInputStream dis = new DataInputStream(is);
	    dis.readFully(source);
	    dis.close();
		
		// create sequence number counter and packet buffer
		int sequenceNo = 0;							// use int for easier incrementing, convert to byte later
		int packetSize = 1024;						// sets size to 1KB
		byte[] packetData = new byte[packetSize+3];	// contains sequenceNo (2 bytes) + last byte index (1 byte) + actual data (1024 bytes)
		
		// set IP and port, open socket
		InetAddress IP = InetAddress.getByName(args[0]);
		int port = Integer.parseInt(args[1]);
		DatagramSocket clientSocket = new DatagramSocket();
		if (printer) System.out.format("Established connection to port %d of %s:\n", port, args[0]);
		
	    while (true) {
	    	
	    	// enough data left to fill a whole packet
	    	if (packetSize*(sequenceNo+1)<source.length) {
	    		
	    		// store sequence number, false last flag, and the data
		    	packetData[0] = (byte) sequenceNo;
		    	packetData[1] = (byte) (sequenceNo >> 8);
		    	packetData[2] = (byte) 0;
		    	System.arraycopy(source, sequenceNo*packetSize, packetData, 3, packetSize);
		    	
		    	// create and send packet
		    	DatagramPacket packet = new DatagramPacket(packetData, packetData.length, IP, port);
			    clientSocket.send(packet);
			    if (printer) System.out.format("Sent packet %d...\n", sequenceNo);
			    
			    // increment packet number and wait
			    sequenceNo++;
				Thread.sleep(10);
				
			// less data left than would otherwise fill a whole packet
	    	} else { 
	    		
	    		// create new byte array with adjusted size to store the remaining data
	    	    int lastPacketSize = source.length-sequenceNo*packetSize;
	    	    byte[] lastPacketData = new byte[lastPacketSize+3];
	    	    
	    	    // store sequence number, true last flag, and the data
		    	lastPacketData[0] = (byte) sequenceNo;
		    	lastPacketData[1] = (byte) (sequenceNo >> 8);
	        	lastPacketData[2] = (byte) 1;
	        	System.arraycopy(source, sequenceNo*packetSize, lastPacketData, 3, lastPacketSize);
	        	
	        	// create and send packet
	        	DatagramPacket packet = new DatagramPacket(lastPacketData, lastPacketData.length, IP, port);
			    clientSocket.send(packet);
			    if (printer) System.out.format("Sent packet %d...\n", sequenceNo);
			    
			    // close socket
			    clientSocket.close();
		    	if (printer) System.out.println("All packets sent.");
		    	break;
	    	}
	    }
	}
}