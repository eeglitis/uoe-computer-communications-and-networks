/* Edgars Eglitis 1353184 */

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.*;

public class Sender1b {
	
	static boolean printer = false; // flag for various printing statements
	
	// storeData method: sets the packet sequence number and last flag in a byte array, also returning it
	public static byte[] storeData(byte[] packetData, int last) {
		if (packetData[1] != (byte) 0) packetData[1] = (byte) 0;
		else packetData[1] = (byte) 1;
    	packetData[2] = (byte) last;
		return packetData;
	}
	
	// sendPacket method: creates and sends a packet, also returning it
	public static DatagramPacket sendPacket(byte[] packetData, DatagramSocket clientSocket, InetAddress IP, int port) throws IOException {
    	DatagramPacket packet = new DatagramPacket(packetData, packetData.length, IP, port);
	    clientSocket.send(packet);
	    if (printer) System.out.format("Sent packet %d to %s...\n", (int) packetData[1], IP);
		return packet;
	}
	
	// receiveAck method: waits until an ACK is received, resending the packet in case of timeout - the number of resent packets is returned
	// the ACK is assumed to be simply the sequence number of the packet
	// packet resending works up to a 100 times, which is used as a termination mechanism for that packet
	public static int receiveAck(DatagramSocket clientSocket, int timeOut, DatagramPacket ack, DatagramPacket packet) throws IOException {
		clientSocket.setSoTimeout(timeOut);
		int packetsResent = 0;
	    while (true) {
	    	if (packetsResent>=100) break;
		    try {
		    	clientSocket.receive(ack);
		    	// check that the sequence numbers match
		    	if (packet.getData()[1] == ack.getData()[1]) {
		    		if (printer) System.out.format("Packet %d ACK received...\n", packet.getData()[1]);
		    		break;
		    	}
		    	// if we receive wrong ack, continue waiting for the correct one as per specification
		    	
		    // ack not received - resend packet and reset timer
		    } catch (SocketTimeoutException e) { 
			    clientSocket.send(packet);
			    packetsResent++;
			    if (printer) System.out.format("Packet %d ACK was not received; resending...\n", packet.getData()[1]);
			    clientSocket.setSoTimeout(timeOut);
		    }
	    }
	    return packetsResent;
	}
	
	public static void main(String[] args) throws IOException {
		
		// read the entire file as a byte array
		File file = new File(args[2]);
		byte[] source = new byte[(int) file.length()];
		InputStream is = new FileInputStream(file);
	    DataInputStream dis = new DataInputStream(is);
	    dis.readFully(source);
	    dis.close();
		
		// create packet data array
		int packetSize = 1024;						// sets size to 1KB
		byte[] packetData = new byte[packetSize+3];	// contains sequenceNo (2 bytes) + last byte index (1 byte) + actual data (1024 bytes)
		packetData[0] = (byte) 0;					// with only 2 different sequence numbers, only the LSB will be modified, so the 8 MSB can be set in advance
		
		// create ack packet and store the specified RetryTimeout
		byte[] ackData = new byte[2];
		DatagramPacket ack = new DatagramPacket(ackData, ackData.length);
		int timeOut = Integer.parseInt(args[3]);
		
		// set IP and port, open socket
		InetAddress IP = InetAddress.getByName(args[0]);
		int port = Integer.parseInt(args[1]);
		DatagramSocket clientSocket = new DatagramSocket();
		if (printer) System.out.format("Established connection to port %d of %s:\n", port, args[0]);
		
		// create counters for retransmitted packets and all packets
		int packetCount = 0;
		int resentPackets = 0;
		
		// set start point for transfer time
		long start = System.currentTimeMillis();

	    while (true) {
	    	
	    	// enough data to fill a whole packet
	    	if (packetSize*(packetCount+1)<source.length) {	

		    	packetData = storeData(packetData, 0);
		    	System.arraycopy(source, packetCount*packetSize, packetData, 3, packetSize);
		    	DatagramPacket packet = sendPacket(packetData, clientSocket, IP, port);
		    	resentPackets += receiveAck(clientSocket, timeOut, ack, packet);

	    		packetCount++;
	    		
	    	// less data left than would otherwise fill a whole packet
	    	} else {
	    		
	    		// create new byte array with adjusted size to store the remaining data
	    	    int lastPacketSize = source.length-packetCount*packetSize;
	    	    byte[] lastPacketData = new byte[lastPacketSize+3];
	    	    
	        	lastPacketData = storeData(lastPacketData, 1);
	        	System.arraycopy(source, packetCount*packetSize, lastPacketData, 3, lastPacketSize);
	        	DatagramPacket packet = sendPacket(lastPacketData, clientSocket, IP, port);
	        	resentPackets += receiveAck(clientSocket, timeOut, ack, packet);
			  
		    	break;
	    	}
	    }
	    
    	// record the time the last packed arrived
    	long end = System.currentTimeMillis()-start;
    	
	    // close socket and print average throughput
	    clientSocket.close();
	    if (printer) System.out.println("All packets sent.");
	    System.out.println("=================================");
    	System.out.format("Average throughput: %.3f KB/s\n", (float) 1000*(packetCount+1)/end);
    	System.out.format("Number of retransmissions: %d\n", resentPackets);
    	System.out.println("=================================");
	}
}