/* Edgars Eglitis 1353184 */
// ACK implementation consists of simply sending back the sequence number

import java.io.*;
import java.net.*;

public class Receiver2a {
	
	static boolean printer = false; // flag for various printing statements
	
	public static void main(String[] args) throws IOException {
		
		// open a new socket on the provided port
		int port = Integer.parseInt(args[0]);
		DatagramSocket serverSocket = new DatagramSocket(port);
		if (printer) System.out.format("Listening in on port %d:\n", port);
		
		// prepare the input packet buffer
		int packetSize = 1024;
        byte[] data = new byte[packetSize+3];
        DatagramPacket packet = new DatagramPacket(data, data.length);
        
        // set ack size, expected packet to be received
        byte[] ackData = new byte[2];
        int expectedSeqNo = 0;
        
        // prepare file to be written to
        File file = new File(args[1]);
		OutputStream os = new FileOutputStream(file);
	    DataOutputStream dos = new DataOutputStream(os);
        
        while (true) {
        	
        	// receive the packet
            serverSocket.receive(packet);
            data = packet.getData();
            int curSeqNo = (data[1] & 0xFF) + ((data[0] & 0xFF)<<8);
            if (printer) System.out.format("Received packet %d...\n", curSeqNo);
            
            // only write if the sequence number is the same as expected
            if (curSeqNo == expectedSeqNo) {
            	dos.write(data, 3, packet.getLength()-3);
            	expectedSeqNo++;
            	if (printer) System.out.format("Packet %d is what we wanted! Now waiting for packet %d...\n", curSeqNo, expectedSeqNo);
            	// send ack for current packet
                ackData[0] = data[0];
                ackData[1] = data[1];
            } else { // unexpected packet, send ack for last received
            	ackData[0] = (byte) ((expectedSeqNo-1)>>>8);
            	ackData[1] = (byte) ((expectedSeqNo-1) & 0xFF);
            }
            
            // send the ack
            DatagramPacket ack = new DatagramPacket(ackData, ackData.length, packet.getAddress(), packet.getPort());
            serverSocket.send(ack);
            if (printer) System.out.format("Sent ACK for packet %d to port %d of %s...\n", (ackData[1] & 0xFF) + (ackData[0]<<8), packet.getPort(), packet.getAddress());
            
            // if the expected packet has last flag set, close socket
            if ((data[2] == 1) && (curSeqNo == expectedSeqNo-1)) {
            	serverSocket.close();
            	if (printer) System.out.println("All packets received.");
            	break;
            }
        }
        dos.close();
	}
}