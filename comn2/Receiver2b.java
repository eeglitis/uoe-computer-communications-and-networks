/* Edgars Eglitis 1353184 */
// ACK implementation consists of simply sending back the sequence number

import java.io.*;
import java.net.*;

public class Receiver2b {
	
	static boolean printer = false; // flag for various printing statements
	
	public static void main(String[] args) throws IOException {
		
		// open a new socket on the provided port
		int port = Integer.parseInt(args[0]);
		DatagramSocket serverSocket = new DatagramSocket(port);
		if (printer) System.out.format("Listening in on port %d:\n", port);
		
		// prepare the input packet buffer
		int packetSize = 1024;
        byte[] data = new byte[packetSize+3];
        DatagramPacket packet = new DatagramPacket(data, data.length);
        
        // set ack size, expected packet to be received, final packet reference
        byte[] ackData = new byte[2];
        int expectedSeqNo = 0;
        int finalPacket = -2;
        
        // create receival window
        int windowSize = Integer.parseInt(args[2]);
        byte[][] buffer = new byte[windowSize][];
        for (int i=0; i<windowSize; i++) {
        	buffer[i] = null;
        }
        
        // prepare file to be written to
        File file = new File(args[1]);
		OutputStream os = new FileOutputStream(file);
	    DataOutputStream dos = new DataOutputStream(os);
        
        while (true) {
        	
        	// receive the packet
            serverSocket.receive(packet);
            data = packet.getData();
            int curSeqNo = (data[1] & 0xFF) + ((data[0] & 0xFF)<<8);
            if (printer) System.out.format("Received packet %d...\n", curSeqNo);
            
            if (data[2] == 1) finalPacket = curSeqNo; // record what the last packet is, to know when to finish
            
            // only care if the sequence number is within a window either way
            if ((curSeqNo>=expectedSeqNo-windowSize) && (curSeqNo<=expectedSeqNo+windowSize-1)) {
            	
            	// record ack data in any case
                ackData[0] = data[0];
                ackData[1] = data[1];
                
            	// if sequence number matches the immediate one, add it to buffer and iterate writing through window
	            if (curSeqNo==expectedSeqNo) {
	            	
	            	buffer[0] = new byte[packet.getLength()];
	            	System.arraycopy(data, 0, buffer[0], 0, packet.getLength());
	                if (printer) System.out.format("Packet %d is exactly what we wanted!\n", curSeqNo);
	                
	            	// write any buffered packets with no gaps
	            	int shiftBy = 0;
	            	for (int i=0; i<windowSize; i++) {
	            		if (buffer[i]==null) break;
	            		dos.write(buffer[i], 3, buffer[i].length-3);
	            		if (printer) System.out.format("Write buffered packet %d...\n", (buffer[i][1] & 0xFF) + ((buffer[i][0] & 0xFF)<<8));
	            		buffer[i] = null;
	            		shiftBy++;
	            		expectedSeqNo++;
	            	}
	            	// adjust window positions
            		if (printer) System.out.println("Adjusting window positions...");
                	for (int i=0; i<windowSize; i++) {
                		if (i+shiftBy>=windowSize) buffer[i] = null;
                		else buffer[i] = buffer[i+shiftBy];
                	}
	            	
	            // if sequence number is within the current window, add it to buffer
	            } else if (curSeqNo>expectedSeqNo) {
	            	
	            	if (buffer[curSeqNo-expectedSeqNo] == null) {
	            		if (printer) System.out.format("Packet %d is within the window, buffering...\n", curSeqNo);
	            		buffer[curSeqNo-expectedSeqNo] = new byte[packet.getLength()];
	            		System.arraycopy(data, 0, buffer[curSeqNo-expectedSeqNo], 0, packet.getLength());
	            	} else if (printer) System.out.format("Packet %d is within the window, but already buffered...\n", curSeqNo);
	                
	            // if sequence number is up to a window behind the expected packet, nothing additional required
	            } else if (printer) System.out.format("Packet %d is not useful!\n", curSeqNo);
	            
	            // send the ack
	            DatagramPacket ack = new DatagramPacket(ackData, ackData.length, packet.getAddress(), packet.getPort());
	            serverSocket.send(ack);
	            if (printer) System.out.format("Sent ACK for packet %d to port %d of %s...\n", (ackData[1] & 0xFF) + (ackData[0]<<8), packet.getPort(), packet.getAddress());
	            		
	            // if all packets received, close socket
	            if (expectedSeqNo-1 == finalPacket) {
	            	serverSocket.close();
	            	if (printer) System.out.println("All packets received.");
	            	break;
	            }
	            
            // otherwise ignore
            } else if (printer) System.out.format("Packet %d is outside the 2 window range! (%d to %d)\n", curSeqNo, expectedSeqNo-windowSize, expectedSeqNo+windowSize-1);
            
        }
        dos.close();
	}
}
