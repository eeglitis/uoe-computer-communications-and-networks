/* Edgars Eglitis 1353184 */

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.*;
import java.util.TimerTask;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;

public class Sender2b {
	
	static boolean printer = false; // flag for various printing statements
	
	public static byte[] source;								// byte array of source file
	public static int packetSize = 1024;						// sets packet size to 1KB
	public static byte[] packetData = new byte[packetSize+3];	// byte array of packet data: contains sequenceNo (2 bytes) + last byte index (1 byte) + actual data (1024 bytes)
	public static DatagramPacket ack;							// ack packet
	public static int timeOut;									// packet timeout
	public static int windowSize;								// size of the sender window
	public static int recPacket;								// last packet whose ack was received
	public static int nextPacket;								// next packet to be processed for sending
	public static DatagramSocket clientSocket;
	public static InetAddress IP;
	public static int port;
	public static Object lock;									// synchronisation point for continuing sender
	public static Timer t;										// timer replacing setSoTimeout
	public static Map<Integer, TimerThread> window;				// the effective sending window, mapping each packet number to its timer
	
	// SendingThread: responsible for sending new packets and starting the timer
	static class SendingThread extends Thread {
		
		// entry point function - redirects to a new function to reduce try/catch blocks everywhere
		public void run() {
			try {
				sender();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		// main thread functionality
		public void sender() throws IOException, InterruptedException {			
			while (true) {

				int localRecPacket = recPacket; // make sure variable doesn't change mid-execution
				
				// sender functions only when there is enough window space for new data
				if (nextPacket-localRecPacket<=windowSize) {
					
					nextPacket++; // increment in advance for a quicker response from ack receiver
					
					// initialize new TimerThread and add it to map
					TimerThread tt = new TimerThread();
					tt.received = false;
					tt.num = nextPacket-1;
					tt.resent = 0;
					t.schedule(tt, timeOut, timeOut); // set timer on first sending of this packet
					window.put(nextPacket-1, tt);
					
					// if there is enough data to fill a full packet normally
					if (packetSize*(nextPacket)<source.length) {
						makePacket(packetData, nextPacket-1);
						if (printer) System.out.format("SENDER: Sent packet %d to %s...\n", nextPacket-1, IP);
			    		
			    	// less data left than would otherwise fill a whole packet - make last packet
					} else {
						 
			    		// create new byte array with adjusted size, set last flag
			    	    int lastPacketSize = source.length-(nextPacket-1)*packetSize;
			    	    byte[] lastPacketData = new byte[lastPacketSize+3];
			    	    lastPacketData[2] = 1;

						makePacket(lastPacketData, nextPacket-1);
						if (printer) System.out.format("SENDER: Sent LAST packet %d to %s...\n", nextPacket-1, IP);
						break;
					}
					
				// not enough window space for new data - pause until an ack is received
				} else {
					synchronized (lock) {
						if (printer) System.out.format("SENDER: After full window: nextPacket is %d, recPacket is %d. Waiting for acks...\n", nextPacket, localRecPacket);
						lock.wait();
					}
					if (printer) System.out.println("SENDER: Wait over");
				}
			}
		}
	}
	
	// AckReceivingThread: responsible for receiving acks
	static class AckReceivingThread extends Thread {
		
		// entry point function - redirects to a new function to reduce try/catch blocks everywhere
		public void run() {
			try {
				receiver();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		// main thread functionality
		public void receiver() throws IOException {
		    while (true) {
		    	
		    	clientSocket.receive(ack);
		    	int localNextPacket = nextPacket; // make sure variable doesn't change mid-execution
		    	int ackPacketNo = (int) ((ack.getData()[1] & 0xFF)+((ack.getData()[0] & 0xFF)<<8));
		    	if (printer) System.out.format("REC: Packet %d ACK received...\n", ackPacketNo);
		    	
		    	// check that the received packet matches any of the sent and unacked packets
		    	if ((ackPacketNo>recPacket) && (ackPacketNo<localNextPacket)) {
		    		
		    		// unconditionally cancel packet's timer and flag it as received
	    			window.get(ackPacketNo).cancel();
	    			window.get(ackPacketNo).received = true;
	    			
		    		// nothing additional required if packet is within window but not immediate
		    		if (ackPacketNo!=recPacket+1) {
		    			if (printer) System.out.format("REC: Packet %d is within window, but not immediate. Cancel timer.\n", ackPacketNo);

		    		// if packet is immediate, adjust recPacket and window, allow sender to continue
		    		} else {
		    			if (printer) System.out.format("REC: Packet %d is the immediately required, shift window and adjust timer array.\n", ackPacketNo);
	
		    			// increment recPacket as required, remove packet from map
		    			int curStartPacket = recPacket+1;
		    			for (int i=curStartPacket; i<curStartPacket+windowSize; i++) {
		    				if (window.get(i) == null) break;
		    				if (!window.get(i).received) break;
		    				window.remove(i);
		    				recPacket++;
		    			}

				    	// sender can now resume with the new recPacket value
				    	synchronized (lock) {
				    		lock.notify();
				    	}
		    		}
			    	
					// if the packet was the last one, finish
	    			if (packetSize*(recPacket+1)>=source.length) { 	
	    				if (printer) System.out.println("REC: Got last packet, ending...");
	    				t.cancel();
	    				break;
	    			}
			    	
			    // if a wrong ack is received, do nothing 
		    	} else {
		    		if (printer) System.out.format("REC: Received ack %d was not useful!\n", ackPacketNo);
		    	}
			    	
			    if (printer) System.out.format("REC: After ack receival: nextPacket is %d, recPacket is %d\n", localNextPacket, recPacket);
		    }
		}
	}
	
	static class TimerThread extends TimerTask {
		
		public int num;					// sequence number of the packet
		public boolean received;		// whether the ack has been received
		public int resent;				// how many times this packet has been resent
		
		// entry point function - redirects to a new function to reduce try/catch blocks everywhere
		public void run() {
			try {
				timer();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// main thread functionality
		public void timer() throws IOException {

	    	resent++;
	    	if (printer) System.out.format("TT: Packet %d ACK was not received; resending...\n", num);
	    	
    		// if there is enough data to fill a full packet normally
    		if (packetSize*(num+1)<source.length) makePacket(packetData, num);
				
			// less data left than would otherwise fill a whole packet - make last packet
    		else {
	    	    int lastPacketSize = source.length-num*packetSize;
	    	    byte[] lastPacketData = new byte[lastPacketSize+3];
	    	    lastPacketData[2] = 1;
				makePacket(lastPacketData, num);
    		}
	    	
	    	// do not allow more than 100 resends
	    	if (resent>=100) {
	    		System.out.println("Packet resent 100 times, ending...");
	    		System.exit(1);
	    	}
		}
	}
	
	// makePacket method: injects the packet sequence number and respective data into the array, makes and sends the packet
	public static void makePacket(byte[] packetData, int nextPacket) throws IOException {
    	packetData[0] = (byte) (nextPacket>>>8);
    	packetData[1] = (byte) (nextPacket & 0xFF);
    	System.arraycopy(source, nextPacket*packetSize, packetData, 3, packetData.length-3);
    	
    	// create and send packet
        DatagramPacket packet = new DatagramPacket(packetData, packetData.length, IP, port);
    	clientSocket.send(packet);
	}
	
	public static void main(String[] args) throws Exception {
		
		// read the entire file as a byte array
		File file = new File(args[2]);
		source = new byte[(int) file.length()];
		InputStream is = new FileInputStream(file);
	    DataInputStream dis = new DataInputStream(is);
	    dis.readFully(source);
	    dis.close();
		
		// create ack packet, store the specified retryTimeout and windowSize
		byte[] ackData = new byte[2];
		ack = new DatagramPacket(ackData, ackData.length);
		timeOut = Integer.parseInt(args[3]);
		windowSize = Integer.parseInt(args[4]);
				
		// set IP and port, open socket
		IP = InetAddress.getByName(args[0]);
		port = Integer.parseInt(args[1]);
		clientSocket = new DatagramSocket();
		if (printer) System.out.format("Established connection to port %d of %s:\n", port, args[0]);
		
		// initialisation steps (also set last flag as 0 by default)
		recPacket = -1;
		nextPacket = 0;
		packetData[2] = 0;
		SendingThread st = new SendingThread();
		AckReceivingThread art = new AckReceivingThread();
		lock = new Object();
		t = new Timer();
		window = new HashMap<Integer, TimerThread>();
		
		// set start point for transfer time
		long start = System.currentTimeMillis();
		
	    st.start();
	    art.start();
	    st.join();
	    art.join();
	    
    	// record the time the last packed arrived
    	long end = System.currentTimeMillis()-start;
    	
	    // close socket and print average throughput
	    clientSocket.close();
	    if (printer) System.out.println("All packets sent.");
	    System.out.println("=================================");
    	System.out.format("Average throughput: %.3f KB/s\n", (float) 1000*(recPacket+1)/end);
    	System.out.println("=================================");
    	System.exit(1);
	}
}
