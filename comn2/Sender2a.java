/* Edgars Eglitis 1353184 */

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.*;
import java.util.TimerTask;
import java.util.Timer;

public class Sender2a {
	
	static boolean printer = false; // flag for various printing statements
	
	public static byte[] source;								// byte array of source file
	public static int packetSize = 1024;						// sets packet size to 1KB
	public static byte[] packetData = new byte[packetSize+3];	// byte array of packet data: contains sequenceNo (2 bytes) + last byte index (1 byte) + actual data (1024 bytes)
	public static DatagramPacket ack;							// ack packet
	public static int timeOut;									// packet timeout
	public static int windowSize;								// size of the sender window
	public static int recPacket;								// last packet whose ack was received
	public static int nextPacket;								// next packet to be processed for sending
	public static DatagramSocket clientSocket;
	public static InetAddress IP;
	public static int port;
	public static Object lock;									// synchronisation point for continuing sender
	public static Timer t;										// timer replacing setSoTimeout
	public static TimerThread tt;								// common timer thread
	public static int basePacket;								// reference for the first packet in current window
	public static int packetsResent;							// counts the number of times resender was invoked
	
	// SendingThread: responsible for sending new packets and starting the timer
	static class SendingThread extends Thread {
		
		// entry point function - redirects to a new function to reduce try/catch blocks everywhere
		public void run() {
			try {
				sender();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		// main thread functionality
		public void sender() throws IOException, InterruptedException {			
			while (true) {

				int localRecPacket = recPacket; // make sure variable doesn't change mid-execution
				
				// sender functions only when there is enough window space for new data
				if (nextPacket-localRecPacket<=windowSize) {
					
					// if there is enough data to fill a full packet normally
					if (packetSize*(nextPacket+1)<source.length) {
						makePacket(packetData, nextPacket);
						if (printer) System.out.format("SENDER: Sent packet %d to %s...\n", nextPacket, IP);
						nextPacket++;
			    		
			    	// less data left than would otherwise fill a whole packet - make last packet
					} else {
						 
			    		// create new byte array with adjusted size, set last flag
			    	    int lastPacketSize = source.length-nextPacket*packetSize;
			    	    byte[] lastPacketData = new byte[lastPacketSize+3];
			    	    lastPacketData[2] = 1;

						makePacket(lastPacketData, nextPacket);
						if (printer) System.out.format("SENDER: Sent LAST packet %d to %s...\n", nextPacket, IP);
						nextPacket++; // still increment nextPacket to work with ack receiver
						break;
					}
					
					// set timer on first invocation, further uses delegated to the ack receiver thread
					if (nextPacket == 1) {
						tt = new TimerThread();
						t.schedule(tt, timeOut, timeOut);
					}
					
				// not enough window space for new data - pause until an ack is received
				} else {
					synchronized (lock) {
						if (printer) System.out.format("SENDER: After full window: nextPacket is %d, recPacket is %d. Waiting for acks...\n", nextPacket, localRecPacket);
						lock.wait();
					}
					if (printer) System.out.println("SENDER: Wait over");
				}
			}
		}
	}
	
	// AckReceivingThread: responsible for receiving acks
	static class AckReceivingThread extends Thread {
		
		// entry point function - redirects to a new function to reduce try/catch blocks everywhere
		public void run() {
			try {
				receiver();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		// main thread functionality
		public void receiver() throws IOException {
		    while (true) {
		    	
		    	clientSocket.receive(ack);
		    	int localNextPacket = nextPacket; // make sure variable doesn't change mid-execution
		    	int ackPacketNo = (int) ((ack.getData()[1] & 0xFF)+((ack.getData()[0] & 0xFF)<<8));
		    	if (printer) System.out.format("REC: Packet %d ACK received...\n", ackPacketNo);
		    	
		    	// check that the received packet matches any of the sent packets		    	
		    	if ((ackPacketNo>recPacket) && (ackPacketNo<localNextPacket)) {
	    			recPacket = ackPacketNo; 	// increase window up to the received packet number
	    			
	    			// reset timer
	    			tt.cancel();
					tt = new TimerThread();
					t.schedule(tt, timeOut, timeOut);
					if (printer) System.out.println("REC: Timer reset");

			    	// sender can now resume with the new recPacket value
			    	synchronized (lock) {
			    		lock.notify();
			    	}
			    	
					// if the packet was the last one, finish
	    			if (packetSize*(recPacket+1)>=source.length) { 	
	    				if (printer) System.out.println("REC: Got last packet, ending...");
	    				tt.cancel();
	    				t.cancel();
	    				break;
	    			}
			    	
			    // if a wrong ack is received, do nothing 
		    	} else {
		    		if (printer) System.out.format("REC: Received ack %d was not useful!\n", ackPacketNo);
		    	}
			    	
			    if (printer) System.out.format("REC: After ack receival: nextPacket is %d, recPacket is %d\n", localNextPacket, recPacket);
		    }
		}
	}
	
	static class TimerThread extends TimerTask {
		
		// entry point function - redirects to a new function to reduce try/catch blocks everywhere
		public void run() {
			try {
				timer();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// main thread functionality
		public void timer() throws IOException {
			
	    	int localNextPacket = nextPacket; 	// make sure variable doesn't change mid-execution
	    	int localRecPacket = recPacket;		// make sure variable doesn't change mid-execution

	    	// reset resent counter upon window change
	    	if (basePacket!=localRecPacket+1) {
	    		basePacket = localRecPacket+1;
	    		packetsResent = 0; 
	    	}
    		packetsResent++;
			
	    	// iterate through current window size and resend everything
	    	for (int p = localRecPacket+1; p<localNextPacket; p++) {
		    	if (printer) System.out.format("TT: Packet %d ACK was not received; resending...\n", p);
		    	
	    		// if there is enough data to fill a full packet normally
	    		if (packetSize*(p+1)<source.length) makePacket(packetData, p);
					
				// less data left than would otherwise fill a whole packet - make last packet
	    		else {
		    	    int lastPacketSize = source.length-p*packetSize;
		    	    byte[] lastPacketData = new byte[lastPacketSize+3];
		    	    lastPacketData[2] = 1;
					makePacket(lastPacketData, p);
	    		}
	    	}
	    	
	    	// do not allow more than 100 resends
	    	if (packetsResent>=100) {
	    		System.out.println("Packet resent 100 times, ending...");
	    		System.exit(1);
	    	}
		}
	}
	
	// makePacket method: injects the packet sequence number and respective data into the array, makes and sends the packet
	public static void makePacket(byte[] packetData, int nextPacket) throws IOException {
    	packetData[0] = (byte) (nextPacket>>>8);
    	packetData[1] = (byte) (nextPacket & 0xFF);
    	System.arraycopy(source, nextPacket*packetSize, packetData, 3, packetData.length-3);
    	
    	// create and send packet
        DatagramPacket packet = new DatagramPacket(packetData, packetData.length, IP, port);
    	clientSocket.send(packet);
	}
	
	public static void main(String[] args) throws Exception {
		
		// read the entire file as a byte array
		File file = new File(args[2]);
		source = new byte[(int) file.length()];
		InputStream is = new FileInputStream(file);
	    DataInputStream dis = new DataInputStream(is);
	    dis.readFully(source);
	    dis.close();
		
		// create ack packet, store the specified retryTimeout and windowSize
		byte[] ackData = new byte[2];
		ack = new DatagramPacket(ackData, ackData.length);
		timeOut = Integer.parseInt(args[3]);
		windowSize = Integer.parseInt(args[4]);
		
		// initialisation steps (also set last flag as 0 by default)
		recPacket = -1;
		nextPacket = 0;
		packetData[2] = 0;
		basePacket = -1;
		packetsResent = 0;
		
		// set IP and port, open socket
		IP = InetAddress.getByName(args[0]);
		port = Integer.parseInt(args[1]);
		clientSocket = new DatagramSocket();
		if (printer) System.out.format("Established connection to port %d of %s:\n", port, args[0]);
		
		// open the execution threads, make lock object, timer, timer task
		SendingThread st = new SendingThread();
		AckReceivingThread art = new AckReceivingThread();
		tt = new TimerThread();
		t = new Timer();
		lock = new Object();
		
		// set start point for transfer time
		long start = System.currentTimeMillis();
		
	    st.start();
	    art.start();
	    st.join();
	    art.join();
	    
    	// record the time the last packed arrived
    	long end = System.currentTimeMillis()-start;
    	
	    // close socket and print average throughput
	    clientSocket.close();
	    if (printer) System.out.println("All packets sent.");
	    System.out.println("=================================");
    	System.out.format("Average throughput: %.3f KB/s\n", (float) 1000*(recPacket+1)/end);
    	System.out.println("=================================");
    	System.exit(1);
	}
}